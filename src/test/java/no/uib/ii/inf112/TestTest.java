package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int a = text.length();
			if (width < a) {
				throw new IllegalArgumentException("text cant be larger than width");
			}
			int b = width - a;
			if (!((b)%2 == 0)) {
				throw new IllegalArgumentException("can't be centered");
			}
			String space = " ".repeat(b/2);
			String result = (space + text + space);
			return result;
		}

		public String flushRight(String text, int width) {
			int textsize = text.length();

			if (width < textsize) {
				throw new IllegalArgumentException("text cant be larger than width");
			}
			int numSpaces = width - textsize;
			String space = " ".repeat(numSpaces);
			String result = (space + text);

			return result;
		}

		public String flushLeft(String text, int width) {
			int textsize = text.length();

			if (width < textsize) {
				throw new IllegalArgumentException("text cant be larger than width");
			}
			int numSpaces = width - textsize;
			String space = " ".repeat(numSpaces);
			String result = (text + space);

			return result;
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};

	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
	}

	@Test
	void testCenterException() {
		IllegalArgumentException thrown = assertThrows(
				IllegalArgumentException.class,
				() -> aligner.center("foo", 6),
				"can't be centered"
		);

		assertTrue(thrown.getMessage().contains("centered"));
	}

}